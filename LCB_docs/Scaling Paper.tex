%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Large Colored Title Article
% LaTeX Template
% Version 1.1 (25/11/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}	 % A4 paper and 11pt font size
\usepackage[english]{babel} % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype} % Better typography
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[svgnames]{xcolor} % Enabling colors by their 'svgnames'
\usepackage{booktabs} % Horizontal rules in tables
\usepackage[margin=1.0in]{geometry}
\usepackage{xcolor}
\usepackage{float}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{lastpage} % Used to determine the number of pages in the document (for "Page X of Total")

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------

\begin{document}

\begin{center}
    \Large
    \textbf{Computational Spray Combustion Behavior with Complex Fuels \\ Code Performance and Scaling}
    
    \vspace{0.4cm}
    \large
        
    \vspace{0.4cm}
    \textbf{Principal Investigator: Perrine Pepiot \\ Co-investigator: Lara Backer}

\end{center}

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------

\section{Overview}
The MPI implementation of the code and use of elliptic solvers make the NGA code a good candidate for large parallelized simulations on high performance computers. The most relevant previous in-depth scaling studies (cite: Capecelatro) have been performed using NGA using the Euler-Lagrange droplet tracking method that will be used in the turbulent spray portion of the proposed runs. These results were performed on the Red Mesa supercomputer, the National Renewable Energy Laboratory (NREL) high performance computing system at Sandia National Laboratories. These results show that NGA has good scaling results, and potential to perform well in similar large scale simulations. These Red Mesa scaling studies, however, did not include the droplet evaporation or chemistry aspect of the code necessary for combustion simulations, which is a significant portion of the computational time. Therefore, simulations using both Euler-Lagrange droplets, droplet evaporation, and combustion have been run using a limited (50,000 SU) allocation on Stampede, and demonstrate the scalability of the entire range of the code to be used in proposed simulations.

%\section{NGA Scaling on Red Mesa}
%The Red Mesa system consists of 15,360 cores and a peak performance of 180TFlops. Reference simulations (cite Capecelatro) are for a 3-dimensional, periodic in two directions, fluidized bed of particles. The Euler-Lagrange method outlined in the proposal, and a collision model for the particles are used, although there is no combustion. The simulations have 134 million cells, and 382 million particles on 4096 cores. The number of cores is reduced to obtain speed up data, and scale up data is obtained by reducing the domain size and number of cores in the vertical direction. Processors are spread in two directions to avoid load imbalances. 
%
%Scale up (also known as weak scaling) is computed as:
%\begin{equation}
%\text{Scale up = $N_{core}^{ref}(\frac{n_xn_yn_z}{t_{step}})(\frac{n_x^{ref}n_y^{ref}n_z^{ref}}{t_{step}^{ref}})^{-1}$}
%\label{eq:scaleup}
%\end{equation} 
%
%Where t is the time step used, N is the number of cores used, n is the number of cells in a given direction, and ref is the reference simulation. Speed up (also known as strong scaling) is computed as:
%\begin{equation}
%\textrm{Speed up} = N_{core}^{ref}(\frac{t_{step}^{ref}}{t_{step}})
%\label{eq:speedup}
%\end{equation} 
%
%Scaling analysis of these runs is shown in Figure \ref{fig:mesascaling}.
%
%\begin{figure}[H]
%\centering
%\begin{subfigure}{.5\textwidth}
%  \centering
%  \includegraphics[width=.8\linewidth]{mesa_speedup}
%  \caption{Speed-up}
%  \label{fig:mesaspeed}
%\end{subfigure}%
%\begin{subfigure}{.5\textwidth}
%  \centering
%  \includegraphics[width=0.8\linewidth]{mesa_scaleup}
%  \caption{Scale-up}
%  \label{fig:mesascale}
%\end{subfigure}
%\caption{A figure with two subfigures}
%\label{fig:mesascaling}
%\end{figure}
%
%An analysis of the timing of the Euler-Lagrange particle tracking algorithms was also done, shown in Figure \ref{fig:mesatimings}. It was found that the timing of the Lagrangian particle tracking routine took up roughly 17\% of the time step, and communication took up roughly 2\%. The collision process dominated the time step, but again, collisions will not be present in our allocation (Perrine - should I even include this, because we aren't using collisions?). Scaling was found to be good up to the maximum number of cores used, 4096, with increasing efficiency for higher loading of cells and particles per processor. The overall cost of the Lagrangian particle tracking routines (including collisions) was found to be approximately 83\% of the simulation cost, and approximately constant throughout the simulations, with the pressure solver at 14\% and velocity solver at 2.4\% (Cite: Capecelatro, this last sentence is very similar to one used in his paper). 
%
%\begin{figure}[H]
%\centering
%  \centering
%  \includegraphics[width=.6\linewidth]{mesa_timings}
%  \caption{Speed-up}
%  \label{fig:mesatimings}
%  \end{figure}

\section{Scaling on Stampede}
The University of Texas TACC's Stampede is a high performance computing system intended primarily for parallel applications scalable to tens of thousands of cores. 

The scaling on Stampede was performed using the limited start-up allocation, in order to test the scaling of NGA for a turbulent spray combustion scenario almost identical to those that will be performed in the simulations if the research allocation is granted. A reduced chemical mechanism for n-heptane combustion was created using the code YARC with the DRGEP method (cite), and was comprised of 71 species and 942 reactions. This mechanism size is approximately (within 10 species of) the size of the mechanism that would be used for the desired jet fuel combustion simulations, and so yields a good method to compute the requested resources. Several simulations were performed to demonstrate both weak and strong scalability of NGA for turbulent spray combustion. The input parameters and resulting graphs were calculated using equations \ref{eq:scaleup} and \ref{eq:speedup}, where speed up is taken at a constant problem size, and scale up uses the same problem size on each core. Scaling parameters used in these simulations can be found in Table \ref{tab:inputs}. \\ 

\begin{table}[H]
\centering
\begin{tabular}{@{}lll@{}}
\toprule
                               & Cells                 & Cores                 \\ \midrule
\multicolumn{1}{l|}{Scale up} & $64^3$& 16 \\ \midrule
\multicolumn{1}{l|}{}         & $64^3$& 64  \\ \midrule
\multicolumn{1}{l|}{}         & $64^3$& 256  \\ \midrule
\multicolumn{1}{l|}{Speed up} & $32^3$& 8 \\ \midrule
\multicolumn{1}{l|}{}         & $64^3$& 64 \\ \midrule
\multicolumn{1}{l|}{}         & $96^3$& 216 \\ \midrule
\bottomrule
\end{tabular}
\caption{NGA Scaling Inputs}
\label{tab:inputs}
\end{table}

Five simulations were run with the conditions in Table \ref{tab:inputs}, as one simulation is a duplicate and used in both speed-up and scale-up tests. In these simulations, the domain was resized based on the number of cells used, so that the same chemistry would occur across simulations, and the flame front would be sufficiently resolved. Cores were varied in one spanwise direction. \\ \\

Simulations were initialized with a high temperature field and already present intermediate species in the gas phase, based on the temperature and composition at ignition from a computed 0-D combustion case at an equivalence ratio of 1.0 with the same mechanism, performed using the FlameMaster code (cite). This was because combustion was found to occur at approximately 32$\mu s$ in the FlameMaster case, which would take far more computational time than is available in the allocated amount just to reach ignition in a single simulation, not even including the time for fuel mass fractions to become present in the gas due to evaporation of the droplets. By initializing the simulation at ignition conditions, only 6 time steps were required to obtain the timings of each time step of a fully combusting simulation. \\ \\

Heptane droplets of $20 \mu m$ diameter at 350K were used in every simulation, with the number of droplets varying such that the fuel-oxidizer equivalence ratio remained at 1, ensuring that similar combustion phenomena would occur across all cases. As a result, simulations have between 2 to 600 droplets. However, as it was found that the Lagrangian droplet solver takes at most 0.1\% of the total timestep, even with the maximum number of droplets used, any droplet scaling by number of droplets in the simulation can be neglected. \\ \\

From the results, it can be seen that the combustion occupies 91 percent of the overall cost of one time step, as shown in Figure \ref{fig:pies}; 99 percent with post processing neglected. 

\begin{figure}[H]
\centering
  \centering
  \includegraphics[width=.6\linewidth]{pichart.png}
  \caption{Proportion of the total time taken by time step}
  \label{fig:pies}
\end{figure}

The presence of a Poisson equation for the pressure means that all cells in the domain are coupled, the speed-up performance is expected to be hindered, although it is clear that the scaling of combustion is of the most importance to the system scalability. However, the combustion chemistry is handled using the stiff variable-coefficient DVODE (cite) implicit solver for the chemistry source terms, as implicit solvers are able to integrate over larger time steps, compared to explicit solvers which require the time step to be on the order of the smallest time step of the reactions. However, using this type of solver leads to a large number of evaluations of the Jacobian matrix for the ODE system. The inclusion of an analytic Jacobian solver for the proposed simulations, is currently being added to the code. The possible addition of the Jacobian solver will result in a slightly decreasing computational cost, although it has not been implemented or tested yet. \\ \\

The scaling results are shown in Figure \ref{fig:stampedes}. (note: scale up set of tests are current marvin scaling)
\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.8\linewidth]{stampedespeedup.png}
  \caption{Speed-up (Strong scaling) of NGA on Stampede}
  \label{fig:sspeed}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{stampedescaleup.png}
  \caption{Scale-up (Weak scaling) of NGA on Stampede}
  \label{fig:sscale}
\end{subfigure}
\caption{Scaling Tests on Stampede}
\label{fig:stampedes}
\end{figure}

Linear scaling is shown with dashed lines as a comparison. Weak scaling results show that NGA has a weak scaling efficiency of approximately 75\% when run on 216 cores (Perrine - why would this be the case? Because combustion is the primary component, this scaling must be related to combustion, however, how would combustion affect timings based on increasing the number of cells present? Should I also create a graph with a break down of just the combustion scaling?) This scaling shows that NGA has good potential to handle the large, time intensive simulations proposed in the main document. \\

The reduced time was also calculated according to equation \ref{eq:redt}. It is a measure of the scaling as a function of the cells per core, and so should be constant for linear scaling. Figure \ref{fig:redtime} (marvin cases!) shows that the reduced time tends towards a constant value of approximately 30ms for a large number of cells per core.
\begin{equation}
\textrm{Reduced time} = \frac{N_{core}t_{step} (ms)}{n_xn_yn_z}
\label{eq:redt}
\end{equation}

\begin{figure}[H]
\centering
  \centering
  \includegraphics[width=.6\linewidth]{reducedtime.png}
  \caption{Reduced time (ms) as a function of cells per core}
  \label{fig:redtime}
\end{figure}

\section{Other Available Resources}
\textbf{Currently, we have no access to large-scale computing systems needed to perform the desired simulations}. Several of the computational fluid dynamics research groups at Cornell have access and share a smaller system, Marvin, which has 1000 cores, but while this resource is viable for testing, it is not possible to perform runs on the scale desired for computational combustion simulations. Other researchers have used the NGA code on other supercomputers previously, such as TACC Stampede and Ranger, Red Mesa at Sandia National Laboratories, NICS Kraken, ALCF Mira, LLNL ALC and Sierra, and OLCF Titan. In addition to the recent simulations on Red Mesa and Stampede, other previous scaling simulations using NGA on NICS Kraken and Sandia's Red Mesa with multiphase flows, but without Lagrangian particle tracking methods and combustion, have shown a weak scaling efficiency above 70\%, corresponding to the results of this scaling study.

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

\begin{thebibliography}{99} % Bibliography - this is intentionally simple in this template
   .
\end{thebibliography}

%----------------------------------------------------------------------------------------

\end{document}