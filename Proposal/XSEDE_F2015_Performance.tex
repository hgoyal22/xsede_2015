\documentclass[11pt]{article}
\usepackage[english]{babel}

% Packages
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{fullpage}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{fancyhdr}
\usepackage{enumitem}
\usepackage{color}
\usepackage{framed}
\usepackage{nicefrac}
\usepackage{units}
\usepackage{amstext}
\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage{cite}
\usepackage{color}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{psfrag}
\usepackage{subfigure}
\usepackage{array}
\usepackage{threeparttable}
\usepackage{dcolumn}
\newcolumntype{d}{D{.}{.}{-1}}
\usepackage{enumitem}
\usepackage{titlesec}
\usepackage{float}
\usepackage{xfrac}
\usepackage{pdfpages}
\usepackage{url}

% Margins
\setlength{\oddsidemargin}{0pt}
\setlength{\topmargin}{-0.3in}
\setlength{\headheight}{0in}
\setlength{\headsep}{30pt}
\setlength{\topskip}{0pt}
\setlength{\textheight}{8.75in}
\setlength{\textwidth}{6.5in}
\setlength{\hoffset}{0pt}
\setlength{\voffset}{0pt}

% My definitions
\def\bm#1{\mbox{\boldmath{$#1$}}}
\DeclareMathAlphabet{\bbm}{U}{bbm}{m}{sl}
\newcommand\etc{etc.\ }
\newcommand\eg{e.g.\ }
\newcommand\ie{i.e.\ }
\newenvironment{myindentpar}[1]{\begin{list}{}{\setlength{\leftmargin}{0.5in}}\item[#1]}{\end{list}}
\renewcommand{\v}[1]{{\mbox{\boldmath $#1$}}}
\newcommand{\wh}[1]{\widehat{#1}}
\newcommand{\wt}[1]{\widetilde{#1}}
\newcommand{\ol}[1]{\overline{#1}}
\newcommand{\Gv}{\l^2}
\newcommand{\sig}{\widehat{\sigma}}
\newcommand{\ddt}[1]{\frac{\partial #1}{\partial t}}
\newcommand{\We}{\mathrm{We}}
\newcommand{\eps}{\varepsilon}



% Some colors
\definecolor{light-gray}{gray}{0.95}

% Colors in titles
\titleformat{\section}
{\color{DarkBlue}\normalfont\Large\bfseries}
{\color{DarkBlue}\thesection}{1em}{}

% Start document
\begin{document}

% First page style
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
%\lfoot{\textsc{NSF PD 14-1443 \hspace{0.01in}  \textbar \hspace{0.01in}  \textcolor{gray}{Fluid Dynamics}}}
\rfoot{\textcolor{gray}{\textsc{Page}} \hspace{0.05in} \textbar \hspace{0.01in} \thepage}


\renewcommand{\refname}{\textsc{ \color{DarkBlue} \large{References}}}


% ------------------------ Cover Page ------------------------------------ %


$ $
\vspace{-0.43in}
\begin{center}
\linethickness{0.4mm}
{\color{DarkBlue} \line(1,0){470}}
\end{center}

\begin{center} 
\color{DarkBlue} 
\textbf{ \Large Code Performance and Scaling}\\[5pt]  
\textbf{ \LARGE Detailed chemical processes in complex turbulent flows}\\[15pt]  

%
\large
\textbf{Principal Investigator:} Perrine Pepiot\\ 
\textbf{Co-Investigators:} Lara Backer, Himanshu Goyal, Youwen Liang \\[10pt]
%
\normalsize 
Sibley School of Mechanical and Aerospace Engineering \\ Cornell University %[-10pt]
\end{center}

\vspace{-0.4in}
\begin{center}
\linethickness{0.4mm}
{\color{DarkBlue} \line(1,0){470}}
\end{center}

\vspace{0.2in}
To achieve good parallel efficiency, NGA makes use of the MPI standards. In particular, all I/O operations are performed using advanced MPI routines, allowing to reach optimal I/O performances with state-of-the-art parallel file systems. Three components are especially critical to the NGA code in the context of the simulations proposed in this project: (1) the velocity, pressure, and scalar transport equation solvers, (2) the Lagrangian particle tracking module, and (3) the integration of the various chemical source terms.

\section{Velocity, pressure, and scalar transport equation solvers}
\begin{wrapfigure}[17]{r}{0.45\textwidth}
%	\vspace{-0.1in}
  	\begin{centering}
    	\includegraphics[width=0.50\textwidth]{Pics_Performance/kraken}
  	\end{centering}
	\vspace{-0.2in}
  	\caption{Weak scaling efficiency of NGA on NICS Kraken.}
	\label{kraken}
	\vspace{-0.1 in}
\end{wrapfigure} 
The first critical component is the velocity, pressure, and scalar solvers, which include all routines that solve the Navier-Stokes equations. The velocity solver naturally presents excellent parallel performance, thanks to the careful use of MPI communication, and because exact parallel polydiagonal solvers are used for the implicit formulation~\cite{DesjardinsNGA2008}. The pressure solver, which corresponds to the solution of the pressure Poisson equation,  effectively projects the predicted velocity field onto the subspace of velocity fields that are compatible with the continuity equation. Being elliptic in nature, the parallel performance of the Poisson solver is expected to be a limiting factor when assessing both weak and strong scaling properties. Several algorithms are available for solving the pressure Poisson equation, including in-house Krylov-based solvers~\cite{Vorst2003}, as well as multigrid solvers included in the HYPRE package distributed by Lawrence Livermore National Laboratory's Center for Applied Scientific Computing~\cite{Hypre}. In addition, a fully three-dimensional parallel black-box multigrid solver~\cite{Dendy} optimized for NGA's data structure is implemented in NGA, and has shown excellent performance on a range of applications. For example, scale-up data have been obtained in the past on NICS Kraken (Fig.~\ref{kraken}, showing that NGA's MPI implementation and elliptic solvers lead to a weak scaling efficiency above 70\% on up to 50,000 compute cores.

%\begin{figure}[h!]
%\centering
%  \includegraphics[width=.6\linewidth]{Pics_Performance/kraken}
%  \caption{Weak scaling efficiency of NGA on NICS Kraken}
% \label{kraken}
% \end{figure}

\section{Lagrangian particle tracking module}
\begin{wrapfigure}[13]{r}{0.45\textwidth}
	\vspace{-0.1in}
  	\begin{centering}
    	\includegraphics[width=0.45\textwidth]{Pics_Performance/piepart}
  	\end{centering}
	\vspace{-0.2in}
  	\caption{Proportion of total time taken by time step for a case of fluidization on 134 M cells, 382 M particles, and 4,096 cores.}
	\label{piepart}
	\vspace{-0.2 in}
\end{wrapfigure} 
The Lagrangian particle tracking solver is used for both fluidized bed simulations and turbulent flame simulations using a particle PDF method. Especially in the dense particulate flow simulations, the particle component accounts for a very large fraction of the computational cost, as is shown for example in Fig.~\ref{piepart} for a non-reactive fluidized bed simulation. This very high contribution is explained by the large number of particles (about three times the number of cells): for each of them, a collision term must be computed that requires a list of all neighboring particles, and a set of ordinary differential equations has to be solved, whose characteristic time is much smaller than the flow time step. The fact that the particle equations are decoupled from one another and only depend on the gas phase variables on the cells immediately surrounding them, combined with the high relative cost of the particle module, suggests that good strong scaling properties (speed-up) and weak scaling properties (scale-up) should be obtained. Scale-up is determined by the following formula:
\begin{equation*}
\text{Scale-up} = N_{\rm core}^{ref} \left( \frac{n_x n_y n_z}{t_{\rm step}}\right) \left( \frac{n_x^{\rm ref} n_y^{\rm ref} n_z^{\rm ref}}{t_{\rm step}^{\rm ref}}\right) ^{-1}
\end{equation*}
while speed-up can be computed for a fixed problem size as
\begin{equation*}
\text{Speed-up} = N_{\rm core}^{ref} \frac{t_{\rm step}}{t_{\rm step}^{\rm ref}}
\end{equation*}
Results are shown for both speed-up and scale-up in Fig.~\ref{redmesa}. 
\begin{figure}[h!]
\centering
  \includegraphics[width=.95\linewidth]{Pics_Performance/redmesa}
  \vspace{-0.1in}
  \caption{Scalability results for NGA simulations of fluidized bed reactors.}
 \label{redmesa}
 \end{figure}
 
Those results were obtained on NREL supercomputer Red Mesa, located in Sandia National Laboratories in Albuquerque, NM. Two different loadings are considered, the red dots corresponding to a configuration with half the number of cells and half the number of particles compared to the blue diamond configuration. Speed-up and scale-up results are very good up to 4,096 cores, the maximum number of cores tested. The efficiency increases when increasing the number of cells and particles per core, which is expected since the relative cost of inter-core communication then decreases. The maximum number of cells and particle per core is dictated by memory limitations, a crude analysis indicating that the storage requirement for one particle being roughly the same as that for a grid cell.
 
 \vspace{-0.1in}
\section{Integration of chemical source terms}
Several strategies are combined to accelerate the evaluation of the chemical source terms in NGA:
\begin{itemize}
\setlength\itemsep{1pt}
\item \underline{Direct integration of the chemical kinetic parameters in NGA}, allowing for faster evaluation of the reaction rates expressions (this is in contrast to most reactive flow solvers that use pre-compiled chemistry libraries)
\item \underline{Analytical jacobians:} To leverage the low-Mach number nature of NGA, a stiff ODE solver is required to implicitly integrate the coupled set of kinetic equations, which necessitates the evaluation of the system's Jacobian matrix. In NGA, this is done analytically, which improves the efficiency of the stiff ODE integration by 1) accelerating the evaluation of the Jacobian itself, and 2) decreasing the number of Jacobian evaluations required per time step when compared to the oft-used numerical Jacobian evaluations.
\item \underline{In-Situ Adaptive Tabulation (ISAT) and chemistry load balancing}: ISAT~\cite{POPE:1997hw,LiuyanLuStephenBPope:2009fd} is used in combination with a dynamic load balancing approach~\cite{Gruselle} to minimize the number of source term integrations required per iteration. For cases similar to the simulations proposed here, previous work by Hiremath et al.~\cite{HiremathFallTechMeeting} has shown good scale-up properties and excellent load balancing properties (within a factor of 1.5 of an estimate for the lowest achievable wall clock time) of the ISAT/dynamic load balancing approach.
\end{itemize}

\vspace{-0.1in}
%\newpage
%
%\section{Overview}
%A detailed scale-up study of NGA was previously conducted on the Red Mesa supercomputer during its deployment phase, which will be used to demonstrate NGA's performance on leadership class computing facilities. After discussing the results from the Red Mesa scale-up study, we show a quick scaling study performed on Stampede to demonstrate that scaling trends on Stampede are similar to those seen on Red Mesa. 
%
%
%
%\section{Scaling on Red Mesa}
%Red Mesa is a National Renewable Energy Laboratory high performance computing system located at Sandia National Laboratories in Albuquerque, NM. It is devoted exclusively to research and development for renewable energy applications. The system consists of 1,920 2.93 Ghz dual-socket quad core, Nehalem x5570 processor nodes, for a total of 15,360 cores with a peak performance of 180 TFlops. The nodes are connected via a QDR InfiniBand network arranged as a 3D toroidal mesh. The total aggregate system memory is 23 TB, with more than 1 PB of filesystem space. In addition, it should be noted that the Red Mesa infrastructure makes up over a third of the Red Sky/Red Mesa supercomputing system, which was ranked 10th in the Top500 November 2009 Report~\cite{Top500}. For all simulations ran on Red Mesa, NGA was compiled using the Intel Fortran compiler v. 11.1, and OpenMPI v. 1.4.1 was used for the MPI-2.0 libraries.
%
%
%%
%%
%%\subsection{Simulations Used for Scaling}
%%Scaling tests on Red Mesa were performed on simulations of Diesel fuel injection. This highly turbulent, atomizing case with high density ratio tests the full capabilities of the code. The reference flow parameters are listed in Table 1. These parameters are characteristic of Diesel fuel injection, albeit with increased surface tension and decreased injection velocity to make the simulations numerically tractable. In addition, Table 1 lists the maximum values for each parameter that we were able to reach thanks to the computation resources o?ered by Red Mesa. It should be noted that these maximum parameters do not correspond to a single simulation, but rather to a number of distinct simulations, where either a larger domain, a finer mesh, or different flow parameters were considered.
%%
%%\subsection{Scaling Results}
%%With over twenty different atomization runs conducted on Red Mesa, enough data were generated to explore the scaling properties of NGA. First, the relative cost of the primary components of NGA was explored. Figure 1 shows the percentage of time spent per time step in the multiphase solver, the velocity solver, and the pressure solver, for a 402 million cells simulation on 4,096 cores once the simulation reached a statistically stationary state. It can be seen that together, these three components of the NGA code account
%%
%%
%%\section{Use of Code on Other Resources}
%%Currently, we have no access to large?scale supercomputers. In the past, NGA has ran on a wide range of leadership?class computing facilities, including Argonne?s ALCF Mira, Oakridge?s OLCF Titan, and University of Texas TACC?s Stampede. Previously, we have made extensive use of NICS? Kraken, TACC Ranger, LLNL ALC and Sierra, and NASA Columbia as well. A careful MPI implementation and scalable elliptic solvers [2, 3, 4] allow for large-scale simulations to be performed on these types of high-performance architectures, as seen in Fig. 5, showing a weak scaling efficiency above 70\% on up to 50,000 compute cores on NICS? Kraken.
%%
%%\newpage
%%
%%\section{Overview}
%%The MPI implementation of the code and use of elliptic solvers make the NGA code a good candidate for large parallelized simulations on high performance computers. The most relevant previous in-depth scaling studies (cite: Capecelatro) have been performed using NGA using the Euler-Lagrange droplet tracking method that will be used in the turbulent spray portion of the proposed runs. These results were performed on the Red Mesa supercomputer, the National Renewable Energy Laboratory (NREL) high performance computing system at Sandia National Laboratories. These results show that NGA has good scaling results, and potential to perform well in similar large scale simulations. These Red Mesa scaling studies, however, did not include the droplet evaporation or chemistry aspect of the code necessary for combustion simulations, which is a significant portion of the computational time. Therefore, simulations using both Euler-Lagrange droplets, droplet evaporation, and combustion have been run using a limited (50,000 SU) allocation on Stampede, and demonstrate the scalability of the entire range of the code to be used in proposed simulations.
%%
%%\section{NGA Scaling on Red Mesa}
%%The Red Mesa system consists of 15,360 cores and a peak performance of 180TFlops. Reference simulations (cite Capecelatro) are for a 3-dimensional, periodic in two directions, fluidized bed of particles. The Euler-Lagrange method outlined in the proposal, and a collision model for the particles are used, although there is no combustion. The simulations have 134 million cells, and 382 million particles on 4096 cores. The number of cores is reduced to obtain speed up data, and scale up data is obtained by reducing the domain size and number of cores in the vertical direction. Processors are spread in two directions to avoid load imbalances. 
%%
%%Scale up (also known as weak scaling) is computed as:
%%\begin{equation}
%%\text{Scale up = $N_{core}^{ref}(\frac{n_xn_yn_z}{t_{step}})(\frac{n_x^{ref}n_y^{ref}n_z^{ref}}{t_{step}^{ref}})^{-1}$}
%%\label{eq:scaleup}
%%\end{equation} 
%%
%%Where t is the time step used, N is the number of cores used, n is the number of cells in a given direction, and ref is the reference simulation. Speed up (also known as strong scaling) is computed as:
%%\begin{equation}
%%\textrm{Speed up} = N_{core}^{ref}(\frac{t_{step}^{ref}}{t_{step}})
%%\label{eq:speedup}
%%\end{equation} 
%%
%%Scaling analysis of these runs is shown in Figure \ref{fig:mesascaling}.
%%
%%\begin{figure}[H]
%%\centering
%%\begin{subfigure}{.5\textwidth}
%%  \centering
%%  \includegraphics[width=.8\linewidth]{mesa_speedup}
%%  \caption{Speed-up}
%%  \label{fig:mesaspeed}
%%\end{subfigure}%
%%\begin{subfigure}{.5\textwidth}
%%  \centering
%%  \includegraphics[width=0.8\linewidth]{mesa_scaleup}
%%  \caption{Scale-up}
%%  \label{fig:mesascale}
%%\end{subfigure}
%%\caption{A figure with two subfigures}
%%\label{fig:mesascaling}
%%\end{figure}
%%
%%An analysis of the timing of the Euler-Lagrange particle tracking algorithms was also done, shown in Figure \ref{fig:mesatimings}. It was found that the timing of the Lagrangian particle tracking routine took up roughly 17\% of the time step, and communication took up roughly 2\%. The collision process dominated the time step, but again, collisions will not be present in our allocation (Perrine - should I even include this, because we aren't using collisions?). Scaling was found to be good up to the maximum number of cores used, 4096, with increasing efficiency for higher loading of cells and particles per processor. The overall cost of the Lagrangian particle tracking routines (including collisions) was found to be approximately 83\% of the simulation cost, and approximately constant throughout the simulations, with the pressure solver at 14\% and velocity solver at 2.4\% (Cite: Capecelatro, this last sentence is very similar to one used in his paper). 
%%
%%\begin{figure}[H]
%%\centering
%%  \centering
%%  \includegraphics[width=.6\linewidth]{mesa_timings}
%%  \caption{Speed-up}
%%  \label{fig:mesatimings}
%%  \end{figure}
%%
%%\section{Scaling on Stampede}
%%The University of Texas TACC's Stampede is a high performance computing system intended primarily for parallel applications scalable to tens of thousands of cores. 
%%
%%The scaling on Stampede was performed using the limited start-up allocation, in order to test the scaling of NGA for a turbulent spray combustion scenario almost identical to those that will be performed in the simulations if the research allocation is granted. A reduced chemical mechanism for n-heptane combustion was created using the code YARC with the DRGEP method (cite), and was comprised of 71 species and 942 reactions. This mechanism size is approximately (within 10 species of) the size of the mechanism that would be used for the desired jet fuel combustion simulations, and so yields a good method to compute the requested resources. Several simulations were performed to demonstrate both weak and strong scalability of NGA for turbulent spray combustion. The input parameters and resulting graphs were calculated using equations \ref{eq:scaleup} and \ref{eq:speedup}, where speed up is taken at a constant problem size, and scale up uses the same problem size on each core. Scaling parameters used in these simulations can be found in Table \ref{tab:inputs}. \\ 
%%
%%\begin{table}[H]
%%\centering
%%\begin{tabular}{@{}lll@{}}
%%\hline
%%                               & Cells                 & Cores                 \\ \hline
%%\multicolumn{1}{l|}{Scale up} & $64^3$& 16 \\ \hline
%%\multicolumn{1}{l|}{}         & $64^3$& 64  \\ \hline
%%\multicolumn{1}{l|}{}         & $64^3$& 256  \\ \hline
%%\multicolumn{1}{l|}{Speed up} & $32^3$& 8 \\ \hline
%%\multicolumn{1}{l|}{}         & $64^3$& 64 \\ \hline
%%\multicolumn{1}{l|}{}         & $96^3$& 216 \\ \hline
%%\hline
%%\end{tabular}
%%\caption{NGA Scaling Inputs}
%%\label{tab:inputs}
%%\end{table}
%%
%%Five simulations were run with the conditions in Table \ref{tab:inputs}, as one simulation is a duplicate and used in both speed-up and scale-up tests. In these simulations, the domain was resized based on the number of cells used, so that the same chemistry would occur across simulations, and the flame front would be sufficiently resolved. Cores were varied in one spanwise direction. \\ \\
%%
%%Simulations were initialized with a high temperature field and already present intermediate species in the gas phase, based on the temperature and composition at ignition from a computed 0-D combustion case at an equivalence ratio of 1.0 with the same mechanism, performed using the FlameMaster code (cite). This was because combustion was found to occur at approximately 32$\mu s$ in the FlameMaster case, which would take far more computational time than is available in the allocated amount just to reach ignition in a single simulation, not even including the time for fuel mass fractions to become present in the gas due to evaporation of the droplets. By initializing the simulation at ignition conditions, only 6 time steps were required to obtain the timings of each time step of a fully combusting simulation. \\ \\
%%
%%Heptane droplets of $20 \mu m$ diameter at 350K were used in every simulation, with the number of droplets varying such that the fuel-oxidizer equivalence ratio remained at 1, ensuring that similar combustion phenomena would occur across all cases. As a result, simulations have between 2 to 600 droplets. However, as it was found that the Lagrangian droplet solver takes at most 0.1\% of the total timestep, even with the maximum number of droplets used, any droplet scaling by number of droplets in the simulation can be neglected. \\ \\
%%
%%From the results, it can be seen that the combustion occupies 91 percent of the overall cost of one time step, as shown in Figure \ref{fig:pies}; 99 percent with post processing neglected. 
%%
%%\begin{figure}[H]
%%\centering
%%  \centering
%%  \includegraphics[width=0.6\textwidth]{Pics_Performance/pichart.png}
%%  \caption{Proportion of the total time taken by time step}
%%  \label{fig:pies}
%%\end{figure}
%%
%%The presence of a Poisson equation for the pressure means that all cells in the domain are coupled, the speed-up performance is expected to be hindered, although it is clear that the scaling of combustion is of the most importance to the system scalability. However, the combustion chemistry is handled using the stiff variable-coefficient DVODE (cite) implicit solver for the chemistry source terms, as implicit solvers are able to integrate over larger time steps, compared to explicit solvers which require the time step to be on the order of the smallest time step of the reactions. However, using this type of solver leads to a large number of evaluations of the Jacobian matrix for the ODE system. The inclusion of an analytic Jacobian solver for the proposed simulations, is currently being added to the code. The possible addition of the Jacobian solver will result in a slightly decreasing computational cost, although it has not been implemented or tested yet. \\ \\
%%
%%The scaling results are shown in Figure \ref{fig:stampedes}. (note: scale up set of tests are current marvin scaling)
%%\begin{figure}[H]
%%\centering
%%\begin{subfigure}{0.5\textwidth}
%%  \centering
%%  \includegraphics[width=0.8\textwidth]{Pics_Performance/stampedespeedup.png}
%%  \caption{Speed-up (Strong scaling) of NGA on Stampede}
%%  \label{fig:sspeed}
%%\end{subfigure}%
%%\begin{subfigure}{0.5\textwidth}
%%  \centering
%%  \includegraphics[width=0.8\textwidth]{Pics_Performance/stampedescaleup.png}
%%  \caption{Scale-up (Weak scaling) of NGA on Stampede}
%%  \label{fig:sscale}
%%\end{subfigure}
%%\caption{Scaling Tests on Stampede}
%%\label{fig:stampedes}
%%\end{figure}
%%
%%Linear scaling is shown with dashed lines as a comparison. Weak scaling results show that NGA has a weak scaling efficiency of approximately 75\% when run on 216 cores (Perrine - why would this be the case? Because combustion is the primary component, this scaling must be related to combustion, however, how would combustion affect timings based on increasing the number of cells present? Should I also create a graph with a break down of just the combustion scaling?) This scaling shows that NGA has good potential to handle the large, time intensive simulations proposed in the main document. \\
%%
%%The reduced time was also calculated according to equation \ref{eq:redt}. It is a measure of the scaling as a function of the cells per core, and so should be constant for linear scaling. Figure \ref{fig:redtime} (marvin cases!) shows that the reduced time tends towards a constant value of approximately 30ms for a large number of cells per core.
%%\begin{equation}
%%\textrm{Reduced time} = \frac{N_{core}t_{step} (ms)}{n_xn_yn_z}
%%\label{eq:redt}
%%\end{equation}
%%
%%\begin{figure}[H]
%%\centering
%%  \centering
%%  \includegraphics[width=.6\linewidth]{Pics_Performance/reducedtime.png}
%%  \caption{Reduced time (ms) as a function of cells per core}
%%  \label{fig:redtime}
%%\end{figure}
%%
%%
%%
%%% ======================================== %
%%%% Code performance
%%% ======================================== %
%%\newpage
%%\section{Code performance}
%%A scaling analysis was performed on Red Mesa, the National Renewable Energy Laboratory's (NREL) high performance computing system, consisting 15,360 cores with a peak performance of 180 TFlops. A series of simulations were run to test the parallel performance of NGA using a reference simulation of 134 million cells and 382 million particles on 4096 cores. The number of cores is progressively reduced to obtain the speed-up data, while the scale-up data is obtained by proportionally reducing the domain size and number of cores in the vertical direction while keeping the relative bed height constant at half the reactor length. 
%%
%%To achieve good parallel efficiency, NGA makes use of the MPI-2.0 standards. In particular, all I/O operations are performed using advanced MPI routines, allowing to reach optimal I/O performances with state-of-the-art parallel file systems. Three components are especially critical to the NGA code running the Lagrangian Particle Tracking (LPT) module. The first one is the velocity solver, which consists of the routines that solve the Navier-Stokes equations. The velocity solver naturally presents excellent parallel performance, thanks to the careful use of MPI communication, and because exact parallel polydiagonal solvers are used for the implicit formulation~\cite{DesjardinsNGA2008}. The second one is the pressure solver, which corresponds to the solution of the pressure Poisson equation, which effectively projects the predicted velocity field onto the subspace of velocity fields that are compatible with the continuity equation. Being elliptic in nature, the parallel performance of the Poisson solver is expected to be a limiting factor when assessing both weak and strong scaling properties. Several algorithms are available for solving the pressure Poisson equation, including in-house Krylov-based solvers~\cite{iterativemethod}, as well as multigrid solvers included in the HYPRE package distributed by Lawrence Livermore National Laboratory's Center for Applied Scientific Computing. In addition, a fully three-dimensional parallel black-box multigrid solver~\cite{dendy1982black} optimized for NGA's data structure has also been implemented in NGA, and has shown excellent performance on a range of applications.
%%
%%\begin{wrapfigure}{r}{0.45\textwidth}
%%%	\vspace{-0.1in}
%%  	\begin{centering}
%%    	\includegraphics[width=0.45\textwidth]{Pics_Main/time}
%%  	\end{centering}
%%	\vspace{-0.2in}
%%  	\caption{Proportion of total time taken by time step for a case of fluidization on 134 M cells, 382 M particles, and 4,096 cores.}
%%	\label{fig:time}
%%	\vspace{-0.2 in}
%%\end{wrapfigure} 
%%%%
%%Finally, the last significant component is the LPT module itself. The particle component accounts for a very large fraction of the computational cost, as is shown for example in Fig.~\ref{fig:time}. This very high contribution is explained by the large number of particles (about three times the number of cells): for each of them, a collision term must be computed that requires a list of all neighboring particles, and a ordinary differential equation has to be solved, whose characteristic time is much smaller than the flow time step. The fact that the particle equations are decoupled from one another and only depend on the gas phase variables on the cells immediately surrounding them, combined with the high relative cost of the particle module, suggests that good strong scaling properties (speed-up) and weak scaling properties (scale-up) should be obtained. \\
%%%%
%%Scale-up is determined by the following formula:
%%%%
%%\begin{equation}
%%\text{Scale-up} = N_{core}^{ref}\left( \frac{n_xn_yn_z}{t_{step}}\right)\left( \frac{n_x^{ref}n_y^{ref}n_z^{ref}}{t_{step}^{ref}}\right)^{-1}
%%\end{equation}
%%%%
%%where $N_{core}$ is the number of cores, $t_{step}$ is the time per timestep, $n_i$ represents the number of cells in the $i$th direction, and the "ref" quantities correspond to the reference simulation. Similarly, speed-up is defined as:
%%%%
%%\begin{equation}
%%\text{Speed-up} = N_{core}^{ref}\frac{t_{step}}{t_{step}^{ref}}
%%\end{equation}
%%%%
%%Fig.~\ref{fig:speedup} shows results for both speed-up and scale-up.
%%%%
%%\begin{figure}[h!]
%%      \centering
%%      \subfigure[Speed-up.\label{fig:speed}]{\includegraphics[width=0.45\textwidth]{Pics_Main/speedup}} 
%%      \subfigure[Scale-up.\label{fig:scale}]{\includegraphics[width=0.45\textwidth]{Pics_Main/scaleup}}     
%%       \caption{Scalability results for NGA simulations of fluidized bed reactors. Dashed line indicate linear scaling.}
%%        \label{fig:speedup}
%%\end{figure}
%%
%%\section{Other Available Resources}
%%\textbf{Currently, we have no access to large-scale computing systems needed to perform the desired simulations}. Several of the computational fluid dynamics research groups at Cornell have access and share a smaller system, Marvin, which has 1000 cores, but while this resource is viable for testing, it is not possible to perform runs on the scale desired for computational combustion simulations. Other researchers have used the NGA code on other supercomputers previously, such as TACC Stampede and Ranger, Red Mesa at Sandia National Laboratories, NICS Kraken, ALCF Mira, LLNL ALC and Sierra, and OLCF Titan. In addition to the recent simulations on Red Mesa and Stampede, other previous scaling simulations using NGA on NICS Kraken and Sandia's Red Mesa with multiphase flows, but without Lagrangian particle tracking methods and combustion, have shown a weak scaling efficiency above 70\%, corresponding to the results of this scaling study.
%%
%%
%%%
%%%

%%
\bibliographystyle{unsrt}
{\small
\bibliography{XSEDE_F2015}}
%%
\end{document}  

%
%\\
%\subsection{Description of fluidized bed solver}
%%%
%Reactive gas-solid fluidized bed reactor, used for the biomass gasification, is simulated using an in-house code NGA, which has been previously used in simulating reactive flows~\cite{capecelatro2015numerical}, biomass pyrolysis and gasification~\cite{pepiot2010chemical}, and detailed simulations of numerous configurations~\cite{desjardins2008accurate,desjardins2009spectrally,van2010ghost,desjardins2006modeling,knudsen2008dynamic,knudsen2009general,wang2007prediction,pepiot2010direct,pepiot2012numerical,capecelatro2013euler,capecelatro2014numerical} A brief description of NGA  is provided here.
%
%The flow of solid spherical particles suspended in a variable density, incompressible carrier fluid is solved in an Eulerian-Lagrangian framework, where the displacement of an individual particle $i$ is calculated using Newton's second law of motion:
%%%
%\begin{align}
%\frac{d \bold{u}_p}{d t} = \mathcal{A}^{(i)} + \mathcal{F}_c^{(i)}+g
%\end{align}
%%%
%where $\mathcal{A}$ is the interphase momentum exchange term, and $\mathcal{F}_c$ is the collision force modeled using a modified soft-sphere approach originally proposed by~\cite{cundall1979discrete}. Additional ODEs are defined to calculate the time evolution of the reactive particles' temperature and compositions, along with gas phase product formation:
%%%
%\begin{align}
%\rho_pC_p\frac{d T_p}{d t} &= \mathcal{B}^{(i)} + \dot{\omega}_T,\\
%\frac{d m^{(i)}_{S,k}}{d t} &= \dot{\omega}_{S,k},\\
%\frac{d m^{(i)}_{G,k}}{d t} &= \dot{\omega}_{G,k} = \mathcal{C}^{(i)}_k 
%\end{align}
%%%
%In these equations, $T_p$, $\rho_p$ and $C_p$ are the particle temperature, density, and specific heat capacity, respectively, $m_{S,k}$ is the total mass of solid component $k$, $m_{G,k}$ is the mass of gas phase product $k$ released by the reacting particle, $\dot{\omega}_T$, $ \dot{\omega}_{S,k}$, and $\dot{\omega}_{G,k}$ are mass production rates obtained from the prescribed solid phase chemical kinetic scheme, and $\mathcal{B}$ and $\mathcal{C}^{(i)}_k$  are interphase heat and mass exchange terms. 
%
%To account for the presence of the particle phase in the fluid without requiring to resolve the boundary layers around individual particles, a volume filter is applied to the low Mach number Navier-Stokes equations following~\cite{anderson1967fluid}, thereby replacing the point variables (fluid velocity, pressure, etc.) by smoother, locally filtered fields, as described in~\cite{capecelatro2013euler,capecelatro2014numerical,capecelatro2015numerical}. The relationship between the interphase exchange term seen by the fluid $\tilde{\mathcal{A}}$ , and that seen by an individual particle i, $\mathcal{A}^{(i)}$, is given by
%%%
%\begin{align}
%\epsilon_f \tilde{\mathcal{A}}(\bold{x},t) = \Sigma_{i=1}^k \mathcal{A}^{(i)}(t)G(|\bold{x}-\bold{x}_p^{(i)}|)V_p,
%\end{align}
%%%
%where $\epsilon_f $ is the filtered gas volume fraction, $N_p$ is the total number of particles, $V_p = \pi d_p^3/6$ is the particle volume, and $G$ is a filtering kernel with a characteristic length $\delta_f\gg d_p$, such that $G(r) > 0$ decreases monotonically with increasing $r$, and is normalized such that it integrates to unity. Details on the choice of $G$ and $\delta_f$, and the actual implementation of Eq. 5 is provided in [50]. The fluid interphase heat and mass transfer terms are obtained from $\mathcal{B}$ and $\mathcal{C}_k$ in a similar manner, and appear in the transport equations solved for gas phase temperature and species mass fractions. Mixture-averaged diffusion coefficients are used to accurately describe species diffusion in the gas phase, while the chemical source term is readily available from the prescribed gas phase kinetic mechanism.
%
%This simulation strategy has proven to be very successful for simulating a wide range of dense fluid-solid particulate flows~\cite{capecelatro2013euler}, including catalytic conversion of bio-vapor in turbulent riser reactors, and biomass gasification with the compact chemistry model. For illustration purpose, figure~\ref{fig:example} shows few product species of biomass pyrolysis in FBR using NGA~\cite{ConfAICHE2013a}.
%%%
%\begin{figure}[htbp]
%   \centering
%   \includegraphics[width=0.3\textwidth]{figures/FBR_example} 
%   \caption{Product species scaled by their maximum value for biomass pyrolysis in FBR after 0.5$s$ of the biomass injection}
%   \label{fig:example}
%\end{figure}
